/*
Define a doubly-linked list of heap-allocated strings.
Write functions to insert, find, and delete items from it.
Test them.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAX_STRING_LEN 30

typedef struct Node  {
	char *cadena;
	struct Node* next;
	struct Node* prev;
} Nodo;

typedef struct List {
	Nodo* head;
	Nodo* tail;
} Lista;

void insertBefore(Lista* list, Nodo* nodo, Nodo* nuevo) {
	nuevo->next = nodo;
	if (nodo->prev == NULL) {
		nuevo->prev = NULL;
		list->head = nuevo;
	} else {
		nuevo->prev = nodo->prev;
		nodo->prev->next = nuevo;
	}
	nodo->prev = nuevo;
}

void insertAfter(Lista* list, Nodo* nodo, Nodo* nuevo) {
	nuevo->prev = nodo;
	if (nodo->next == NULL) {
		nuevo->next = NULL;
		list->tail = nuevo;
	} else {
		nuevo->next = nodo->next;
		nodo->next->prev = nuevo;
	}
	nodo->next = nuevo;
}

void insertBeginning(Lista* list, Nodo* newNodo) {
	if (list->head == NULL) {
		list->head = newNodo;
		list->tail = newNodo;
		newNodo->prev = NULL;
		newNodo->next = NULL;
	} else {
		insertBefore(list, list->head, newNodo);
	}
}

void insertEnd(Lista* list, Nodo* newNodo) {
	if (list->tail == NULL) {
		insertBeginning(list, newNodo);
	} else {
		insertAfter(list, list->tail, newNodo);
	}
}

void imprimirForwards(Lista* list) {
	printf("Printing forwards...\n");
	if (list->head == NULL && list == NULL) {
		printf("Unable to print the list forwards!");
		getchar();
		exit(1);
	}

	printf("list->head pointer address: %p\n", list->head);
	if (list->head) {
		printf("list->head->next pointer address: %p\n", list->head->next);
		printf("list->head->prev pointer address: %p\n", list->head->prev);
		printf("list->head->cadena value: %s\n", list->head->cadena);
	}
	
	Nodo* nodoTemporal = list->head;
	
	while (nodoTemporal != NULL) {
		printf("%s\n", nodoTemporal->cadena);
		nodoTemporal = nodoTemporal->next;
	}
}

void imprimirBackwards(Lista* list) {
	printf("Printing backwards...\n");
	if (list->tail == NULL && list == NULL) {
		printf("Unable to print the list backwards!");
		getchar();
		exit(1);
	}
	printf("list->tail pointer address: %p\n", list->tail);
	if (list->tail) {
		printf("list->tail->next pointer address: %p\n", list->tail->next);
		printf("list->tail->prev pointer address: %p\n", list->tail->prev);
		printf("list->tail->cadena value: %s\n", list->tail->cadena);
	}	
	Nodo* temp = list->tail;
	while (temp != NULL) {
		printf("%s\n", temp->cadena);
		temp = temp->prev;
	}
}

void liberarMemoria(Lista* lista) {
	printf("Liberando memoria...\n");
	Nodo* nodoTemp = lista->head;
	while (lista->head != NULL) {		
		nodoTemp = lista->head;
		lista->head = lista->head->next;
		free(nodoTemp);
	}

	/*Nodo* nodoTempTail = lista->tail;
	while (lista->tail != NULL) {
		nodoTempTail = lista->tail;
		lista->tail = lista->tail->prev;
		free(nodoTempTail);
	}*/

	printf("�Memoria liberada!\n");
	imprimirForwards(lista);
	imprimirBackwards(lista);

}

void encontrar(Lista* lista, char* cadena) {
	int indice = 1;
	int encontrada = 0;
	Nodo* temp = lista->head;
	while (temp != NULL && encontrada != 1) {
		printf("Cadena: %.*s", strlen(cadena) - 1, cadena);
		printf(", Cadena actual: %.*s\n", strlen(temp->cadena) - 1, temp->cadena);
		if (strcmp(cadena, temp->cadena) == 0) {
			printf("�Cadena encontrada! En el indice #%d (forwards)", indice);
			encontrada = 1;
			break;
		}
		temp = temp->next;
		indice++;
	}

	if (encontrada == 0) {
		printf("Cadena no encontrada en la lista");
	}
	
}

int main() {
	int opcion;
	Lista* lista = (Lista*)malloc(sizeof(Lista));
	if (lista == NULL) {
		puts("Unable to allocate buffer!");
		exit(1);
	}
	lista->head = NULL;
	lista->tail = NULL;

	do {
		system("@cls||clear");
		printf("LISTA DOBLEMENTE ENLAZADA\n");
		printf("1. Insertar nodo\n");
		printf("2. Encontrar nodo\n");
		printf("3. Borrar nodo\n");
		printf("4. Imprimir lista (Forwards)\n");
		printf("5. Imprimir lista (Backwards)\n");
		printf("6. Salir de programa\n");
		printf("Seleccione una opcion (1-6): ");

		opcion = getchar();
		getchar();

		switch (opcion) {
			case '1': {
				Nodo * nodoNuevo = (Nodo*)malloc(sizeof(Nodo));
				char* valorCadena = (char*)malloc(sizeof(char) * MAX_STRING_LEN);
				if (valorCadena == NULL) {
					puts("Unable to allocate buffer!");
					exit(1);
				}

				printf("Ingrese la cadena de texto: ");
				fgets(valorCadena, MAX_STRING_LEN, stdin);
				nodoNuevo->cadena = valorCadena;
				nodoNuevo->next = NULL;
				nodoNuevo->prev = NULL;
				insertEnd(lista, nodoNuevo);
				break;
			}
			case '2': {				
				char* valorCadena = (char*)malloc(sizeof(char) * MAX_STRING_LEN);
				
				if (valorCadena == NULL) {
					puts("Unable to allocate buffer!");
					exit(1);
				}

				printf("Ingrese la string a buscar: ");
				fgets(valorCadena, MAX_STRING_LEN, stdin);
				encontrar(lista, valorCadena);
				printf("\nPresione cualquier tecla para volver al menu...");
				getchar();				
				break;
			}
			case '3': {
				break;
			}
			case '4': {
				system("@cls||clear");
				printf("LISTA INGRESADA (FORWARDS): \n");
				imprimirForwards(lista);
				printf("\nPresione cualquier tecla para volver al menu...");
				getchar();
				break;
			}
			case '5': {
				system("@cls||clear");
				printf("LISTA INGRESADA (BACKWARDS): \n");
				imprimirBackwards(lista);
				printf("\nPresione cualquier tecla para volver al menu...");
				getchar();
				break;
			}
			case '6': {
				system("@cls||clear");
				liberarMemoria(lista);
				printf("FIN DEL PROGRAMA");
				printf("\nPresione cualquier tecla para salir...");
				break;
			}
			default:
				printf("Opci�n inv�lida: %d", opcion);
				getchar();
		}
	} while (opcion != '6');

	getchar();
	return(0);
}